from unittest import TestCase

from source.rover import Rover, Direction


class TestRover(TestCase):

    def test_get_position_returns_0_0_at_rover_creation(self):
        # Given
        rover = Rover(0, 0, Direction.WEST)
        # When
        position = rover.get_position()
        # Then
        self.assertEqual((0, 0), position)

    def test_get_position_returns_49_80_when_rover_is_created_at_49_80(self):
        # Given
        rover = Rover(49, 80, Direction.WEST)
        # When
        position = rover.get_position()
        # Then
        self.assertEqual((49, 80), position)

    def test_get_direction_returns_direction_given_at_rover_creation(self):
        # Given
        rover = Rover(49, 80, Direction.WEST)
        # When
        direction = rover.get_direction()
        # Then
        self.assertEqual(Direction.WEST, direction)

    def test_move_forward_changes_rover_position_to_0_1_when_rover_is_on_0_0_facing_north(self):
        # Given
        rover = Rover(0, 0, Direction.NORTH)

        # When
        rover.move_forward()

        # Then
        position = rover.get_position()
        direction = rover.get_direction()
        self.assertEqual((0, 1), position)
        self.assertEqual(Direction.NORTH, direction)

    def test_move_forward_changes_rover_position_to_1_0_when_rover_is_on_0_0_facing_east(self):
        # Given
        rover = Rover(0, 0, Direction.EAST)

        # When
        rover.move_forward()

        # Then
        position = rover.get_position()
        direction = rover.get_direction()
        self.assertEqual((1, 0), position)
        self.assertEqual(Direction.EAST, direction)

    def test_move_backward_changes_rover_position_to_0_1_when_rover_is_on_1_1_facing_east(self):
        # Given
        rover = Rover(1, 1, Direction.EAST)

        # When
        rover.move_backward()

        # Then
        position = rover.get_position()
        direction = rover.get_direction()
        self.assertEqual((0, 1), position)
        self.assertEqual(Direction.EAST, direction)

    def test_move_backward_changes_rover_position_to_1_2_when_rover_is_on_1_1_facing_south(self):
        # Given
        rover = Rover(1, 1, Direction.SOUTH)

        # When
        rover.move_backward()

        # Then
        position = rover.get_position()
        direction = rover.get_direction()
        self.assertEqual((1, 2), position)
        self.assertEqual(Direction.SOUTH, direction)

    def test_turn_should_change_direction_to_EAST_when_facing_NORTH_and_turning_right(self):
        # Given
        rover = Rover(0, 0, Direction.NORTH)

        # When
        rover.turn('r')

        # Then
        direction = rover.get_direction()
        self.assertEqual(Direction.EAST, direction)

    def test_turn_should_change_direction_to_EAST_when_facing_south_and_turning_left(self):
        # Given
        rover = Rover(0, 0, Direction.SOUTH)

        # When
        rover.turn('l')

        # Then
        direction = rover.get_direction()
        self.assertEqual(Direction.EAST, direction)

    def test_execute_should_change_rover_position_according_to_command(self):
        # Given
        rover = Rover(0, 0, Direction.NORTH)

        # When
        rover.execute('flbrff')

        # Then
        position = rover.get_position()
        direction = rover.get_direction()
        self.assertEqual((1,3), position)
        self.assertEqual(Direction.NORTH, direction)


    def test_execute_should_change_rover_position_according_to_circle_command(self):
        # Given
        rover = Rover(2, -2, Direction.SOUTH)

        # When
        rover.execute('frfrfrfr')

        # Then
        position = rover.get_position()
        direction = rover.get_direction()
        self.assertEqual((2, -2), position)
        self.assertEqual(Direction.SOUTH, direction)


