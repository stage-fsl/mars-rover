from enum import Enum
from typing import Tuple


class Direction(Enum):
    NORTH = 0
    EAST = 1
    SOUTH = 2
    WEST = 3


class Rover:
    def __init__(self, x: int, y: int, direction: Direction):
        self.x = x
        self.y = y
        self.direction = direction

    def get_position(self) -> Tuple[int, int]:
        return self.x, self.y

    def get_direction(self) -> Direction:
        return self.direction

    def move_forward(self) -> None:
        self._move('f')

    def move_backward(self) -> None:
        self._move('b')

    def _move(self, orientation):
        increment = 1 if orientation == 'f' else -1
        if self.direction == Direction.NORTH:
            self.y += increment
        if self.direction == Direction.EAST:
            self.x += increment
        if self.direction == Direction.WEST:
            self.x -= increment
        if self.direction == Direction.SOUTH:
            self.y -= increment

    def turn(self, orientation):
        offset = 1 if orientation == 'r' else -1
        self.direction = Direction((self.direction.value + offset) % 4)

    def execute(self, sequence):
        for command in sequence:
            if command in 'lr':
                self.turn(command)
            else:
                self._move(command)

